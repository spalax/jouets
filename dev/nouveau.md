# Création d'un nouveau projet

- Exécution

  - ``python -m jouets.monprojet`` doit lancer le projet.
  - ``./bin/monprojet`` aussi.

- Setup

  - Ajouter le projet aux entry points dans setup.cfg.

- Documentation

  - Ajouter le projet à la liste des projets dans :

    - `doc/index.rst`
    - `README.rst`
    - `CHANGELOG.md`

  - Créer la documentation `doc/monprojet.rst`.
  - Créer le changelog `changelogs/monprojet.md`
