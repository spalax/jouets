* Version 0.2.1 (2020-04-27)

    - Déplacement de l'analyseur de ligne de commandes dans son propre module
    - [test] Désactive l'import de turtle lors des tests (car turtle n'est pas forcément installé sur les environnements de test).

    -- Louis Paternault <spalax@gresille.org>

* Version 0.2.0 (2016-07-23)

    * Le programme peut être appelé comme un module python (``python -m jouets.fractale`` en plus de ``fractale``).

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2014-10-25)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
