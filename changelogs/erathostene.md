* Version 0.3.0 (unreleased)

    * Amélioration de l'algorithme, qui est maintenant 3000 fois plus rapide.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.2.1 (2020-04-27)

    * Amélioration des performances
    * Améliorations mineures

    -- Louis Paternault <spalax@gresille.org>

* Version 0.2.0 (2016-07-23)

    * Le programme peut être appelé comme un module python (``python -m jouets.erathostene`` en plus de ``erathostene``).

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2014-09-10)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
