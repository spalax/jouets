* Version 0.2.1 (2020-04-27)

    * Simplification du système de gestion de plugins.
    * Renommage de `base` en `shapes.__init__`.
    * Améliorations mineures de la documentation.
    * [test] Désactive l'import de turtle lors des tests (car turtle n'est pas forcément installé sur les environnements de test)

    -- Louis Paternault <spalax@gresille.org>

* Version 0.2.0 (2016-07-23)

    * Le programme peut être appelé comme un module python (``python -m jouets.labyrinthe`` en plus de ``labyrinthe``).
    * Mineures améliorations de la documentation.
    * Ajout de la possibilité de choisir son template lors de l'export en LaTeX.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2014-08-26)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
