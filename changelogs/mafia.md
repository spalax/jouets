* Version 0.1.1 (2020-04-27)

    - Amélioration de l'aide.
    - Améliorations diverses de la documentation.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2016-07-12)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
