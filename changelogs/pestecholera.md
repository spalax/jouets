* Version 0.2.1 (2020-04-27)

    - [peste] Correction d'un bug faisant que peste plantait avant la simulation..
    - [peste] Déplace l'analyseur de ligne de commande dans son propre module.
    - [test] Désactive l'import de turtle lors des tests (car turtle n'est pas forcément installé sur les environnements de test).

    -- Louis Paternault <spalax@gresille.org>

* Version 0.2.0 (2016-07-23)

    * Renommage de `pesteetcholera` en `pestecholera`.
    * Le programme peut être appelé comme un module python (par exemple, ``python -m jouets.pestecholera.peste`` en plus de ``pestecholera.peste``).

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2014-12-07)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
