* Version 0.2.0 (2016-07-23)

    * Le programme peut être appelé comme un module python (``python -m jouets.sudoku`` en plus de ``sudoku``).
    * Amélioration du rendu.
    * Remplacement des thread par des process: ce programme est maintenant réellement plus rapide en parallèle (avec plusieurs CPU) (#4).
    * Abandon de ``time.perf_counter()``, maintenant obsolète.
    * Diverses mineures améliorations de code et de documentation.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.3 (2014-11-05)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
