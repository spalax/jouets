* Version 0.2.0 (unreleased)

    * Nouveau programme : serpent.
    * Ajout d'un émulateur rudimentaire de microbit.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2019-05-30)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
