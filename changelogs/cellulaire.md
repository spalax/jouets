* Version 0.1.1 (2020-04-27)

    - Amélioration de l'aide

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2019-06-14)

    - Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
