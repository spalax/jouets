* Version 0.2.1 (2020-04-27)

    * Code

      * Simplification du système de plugins.
      * [memobble] Renommage de bissection en bipartite

    * Documentation

      * Ajout d'une mention sur les corps fini (#14)
      * Documentation du mémobble (#2)
      * Améliorations mineures.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.2.0 (2016-07-22)

    * Dobble

        * Changement de la définition d'une configuration triviale.
        * Compléments mathématiques (#9).

    * Mémobble

        * Génération de jeux de cartes de Mémobble
        * Tests (#3)
        * La documentation est encore manquante

    * Diffle

        * Documentation du Diffle (#2)

    * Diverses améliorations de la documentation.
    * Amélioration de la génération des couleurs aléatoires

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2014-10-22)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
