* Version 0.1.0 (2020-04-27)

    * Première version, avec les jeux :

        * pgserpent
        * pglabyrinthe
        * pgenvahisseurs
        * pgslalom

    -- Louis Paternault <spalax@gresille.org>
