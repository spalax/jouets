* Version 0.1.1 (2020-04-27)

  - Les programmes ne s'exécutent pas lorsque le fichier est simplement importé.
  - Utilisation du système de plugin pour rechercher les nouvelles simulations.
  - euler est maintenant un "namespace package" au sens du PEP 420
  - [ressort] Suppression d'une ligne inutile

  -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2019-06-06)

  Première version publiée.

  -- Louis Paternault <spalax@gresille.org>
