* Version 0.2.0 (2016-07-22)

    * Le programme peut être appelé comme un module python (``python -m jouets.aperitif`` en plus de ``aperitif``).
    * Correction d'un bug : entrer une liste de prix contenant 0 ne fait plus planter le programme.
    * Améliorations mineures de la documentation.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2014-08-24)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
