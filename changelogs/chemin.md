* Version 0.2.0 (2016-07-22)

    * Le programme peut être appelé comme un module python (``python -m jouets.chemin`` en plus de ``chemin``).
    * Améliorations mineures de la documentation
    * Remplacement des thread par des process : le programme est maintenant réellement plus rapide en parallèle (avec plusieurs CPU). #4

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2014-09-11)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
