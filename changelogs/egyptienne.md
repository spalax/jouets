* Version 0.2.0 (2016-07-23)

    * Le programme peut être appelé comme un module python (``python -m jouets.egyptienne`` en plus de ``egyptienne``).

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2014-08-25)

    Première version publiée.

    -- Louis Paternault <spalax@gresille.org>
