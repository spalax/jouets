# Copyright 2020 Louis Paternault
#
# This file is part of Jouets.
#
# Jouets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jouets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jouets.  If not, see <http://www.gnu.org/licenses/>.

"""Génère des nombres suivant une loi de probabilité donnée."""


class LoiProba:
    """Loi de probabilité : génère des nombres aléatoires."""

    # pylint: disable=too-few-public-methods

    def random(self):
        """Renvoit un nombre aléatoire, entier supérieur ou égal à 1."""
        raise NotImplementedError()
