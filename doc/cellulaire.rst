`cellulaires` — Automates cellulaires
=====================================

Quelques `automates cellulaires <https://fr.wikipedia.org/wiki/Automate_cellulaire>`__ (en fait, un seul au moment où j'écris ces lignes).

.. toctree::
   :maxdepth: 1

   cellulaire/dimension15
