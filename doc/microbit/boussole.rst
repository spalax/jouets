`boussole` — Indique le nord
============================

Ce programme indique le nord par une flèche. C'est une version moins élégante que celle proposée dans la `documentation officielle <https://microbit-micropython.readthedocs.io/en/v1.0.1/compass.html#example>`__, mais plus simplement compréhensible par des élèves débutants en informatique.

.. literalinclude:: /../microbit/boussole.py
   :language: python
   :linenos:
