`serpent` — Un jeu de serpent
=============================

Ceci est le jeu classique du serpent, sauf que la carte microbit joue toute seule. Elle fait tourner le serpent aléatoirement, la seule condition étant de ne pas foncer dans un mur.

Regarder la carte jouer est étrangement frustrant et addictif.

.. literalinclude:: /../microbit/serpent.py
   :language: python
   :linenos:
