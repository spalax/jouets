..
   Copyright 2023 Louis Paternault
   
   Cette œuvre de Louis Paternault est mise à disposition selon les termes de
   la licence Creative Commons Attribution - Partage dans les Mêmes Conditions
   4.0 International (CC-BY-SA). Le texte complet de la licence est disponible
   à l'adresse : http://creativecommons.org/licenses/by-sa/4.0/deed.fr

.. _latex:

******************************************
`LaTeX` — Bouts de codes amusants en LaTeX
******************************************

J'aime beaucoup utiliser `PGF/TikZ <https://www.ctan.org/pkg/pgf>`__ pour tracer mes figures en :math:`\LaTeX`, car cela me permet :

- en tant qu'informaticien, de programmer mes figures ;
- en tant que mathématicien, d'utiliser un programme de construction pour dessiner.

Voici quelques images, construites en utilisant quelques calculs mathématiques (surtout de la géométrie cartésienne et de la trigonométrie).

.. toctree::
   :maxdepth: 1

   latex/etoiles
   latex/noeuds
