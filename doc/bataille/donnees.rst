============================
Données brutes et Graphiques
============================

.. contents:: Table des matières
   :local:

.. only:: latex

   Les données brutes et les graphiques ne sont pas tous inclus dans ce document. Visitez `le site web <https://jouets.ababsurdo.fr/bataille/donnees>`__.

.. only:: html

   Données brutes
   ==============

   * 1 couleur :
     :download:`bataille-1-2-1000000.csv`
     :download:`bataille-1-3-1000000.csv`
     :download:`bataille-1-4-1000000.csv`
     :download:`bataille-1-5-1000000.csv`
     :download:`bataille-1-6-1000000.csv`
     :download:`bataille-1-7-1000000.csv`
     :download:`bataille-1-8-1000000.csv`
     :download:`bataille-1-9-1000000.csv`
     :download:`bataille-1-10-1000000.csv`
   * 2 couleurs :
     :download:`bataille-2-2-1000000.csv`
     :download:`bataille-2-3-1000000.csv`
     :download:`bataille-2-4-1000000.csv`
     :download:`bataille-2-5-1000000.csv`
     :download:`bataille-2-6-1000000.csv`
     :download:`bataille-2-7-1000000.csv`
     :download:`bataille-2-8-1000000.csv`
     :download:`bataille-2-9-1000000.csv`
     :download:`bataille-2-10-1000000.csv`
   * 3 couleurs :
     :download:`bataille-3-2-1000000.csv`
     :download:`bataille-3-3-1000000.csv`
     :download:`bataille-3-4-1000000.csv`
     :download:`bataille-3-5-1000000.csv`
     :download:`bataille-3-6-1000000.csv`
     :download:`bataille-3-7-1000000.csv`
     :download:`bataille-3-8-1000000.csv`
     :download:`bataille-3-9-1000000.csv`
     :download:`bataille-3-10-1000000.csv`
   * 4 couleurs :
     :download:`bataille-4-2-1000000.csv`
     :download:`bataille-4-3-1000000.csv`
     :download:`bataille-4-4-1000000.csv`
     :download:`bataille-4-5-1000000.csv`
     :download:`bataille-4-6-1000000.csv`
     :download:`bataille-4-7-1000000.csv`
     :download:`bataille-4-8-1000000.csv`
     :download:`bataille-4-9-1000000.csv`
     :download:`bataille-4-10-1000000.csv`
   * 5 couleurs :
     :download:`bataille-5-2-1000000.csv`
     :download:`bataille-5-3-1000000.csv`
     :download:`bataille-5-4-1000000.csv`
     :download:`bataille-5-5-1000000.csv`
     :download:`bataille-5-6-1000000.csv`
     :download:`bataille-5-7-1000000.csv`
     :download:`bataille-5-8-1000000.csv`
     :download:`bataille-5-9-1000000.csv`
     :download:`bataille-5-10-1000000.csv`
   * 6 couleurs :
     :download:`bataille-6-2-1000000.csv`
     :download:`bataille-6-3-1000000.csv`
     :download:`bataille-6-4-1000000.csv`
     :download:`bataille-6-5-1000000.csv`
     :download:`bataille-6-6-1000000.csv`
     :download:`bataille-6-7-1000000.csv`
     :download:`bataille-6-8-1000000.csv`
     :download:`bataille-6-9-1000000.csv`
     :download:`bataille-6-10-1000000.csv`
   * 7 couleurs :
     :download:`bataille-7-2-1000000.csv`
     :download:`bataille-7-3-1000000.csv`
     :download:`bataille-7-4-1000000.csv`
     :download:`bataille-7-5-1000000.csv`
     :download:`bataille-7-6-1000000.csv`
     :download:`bataille-7-7-1000000.csv`
     :download:`bataille-7-8-1000000.csv`
     :download:`bataille-7-9-1000000.csv`
     :download:`bataille-7-10-1000000.csv`
   * 8 couleurs :
     :download:`bataille-8-2-1000000.csv`
     :download:`bataille-8-3-1000000.csv`
     :download:`bataille-8-4-1000000.csv`
     :download:`bataille-8-5-1000000.csv`
     :download:`bataille-8-6-1000000.csv`
     :download:`bataille-8-7-1000000.csv`
     :download:`bataille-8-8-1000000.csv`
     :download:`bataille-8-9-1000000.csv`
     :download:`bataille-8-10-1000000.csv`
   * 9 couleurs :
     :download:`bataille-9-2-1000000.csv`
     :download:`bataille-9-3-1000000.csv`
     :download:`bataille-9-4-1000000.csv`
     :download:`bataille-9-5-1000000.csv`
     :download:`bataille-9-6-1000000.csv`
     :download:`bataille-9-7-1000000.csv`
     :download:`bataille-9-8-1000000.csv`
     :download:`bataille-9-9-1000000.csv`
     :download:`bataille-9-10-1000000.csv`
   * 10 couleurs :
     :download:`bataille-10-2-1000000.csv`
     :download:`bataille-10-3-1000000.csv`
     :download:`bataille-10-4-1000000.csv`
     :download:`bataille-10-5-1000000.csv`
     :download:`bataille-10-6-1000000.csv`
     :download:`bataille-10-7-1000000.csv`
     :download:`bataille-10-8-1000000.csv`
     :download:`bataille-10-9-1000000.csv`
     :download:`bataille-10-10-1000000.csv`

   Graphiques
   ==========

   1 couleur
   ---------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(1, 10, nombre=1000000)

   2 couleurs
   ----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(2, 10, nombre=1000000)

   3 couleurs
   ----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(3, 10, nombre=1000000)

   4 couleurs
   ----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(4, 10, nombre=1000000)

   5 couleurs
   ----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(5, 10, nombre=1000000)

   6 couleurs
   ----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(6, 10, nombre=1000000)

   7 couleurs
   ----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(7, 10, nombre=1000000)

   8 couleurs
   ----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(8, 10, nombre=1000000)

   9 couleurs
   ----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(9, 10, nombre=1000000)

   10 couleurs
   -----------

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 2, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 3, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 4, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 5, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 6, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 7, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 8, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 9, nombre=1000000)

   .. plot::

      from jouets.bataille.graphiques import pairimpair
      pairimpair(10, 10, nombre=1000000)
