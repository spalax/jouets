.. _api-anagrammes:

`anagrammes`
============

.. currentmodule:: jouets.anagrammes

Classes définies pour la :ref:`recherche d'anagrammes <anagrammes>`.

Alphabet
--------

.. autoclass:: Alphabet
   :members:

Intervalle
----------

.. autoclass:: Intervalle
   :members:

Dictionnaire arborescent
------------------------

Le dictionnaire arborescent est décrit plus en détail sur la page de :ref:`l'outil de recherche d'anagrammes <anagrammes>`.

.. autoclass:: DictionnaireArborescent
   :members:

   .. automethod:: _anagrammes

   .. automethod:: _multi_anagrammes
