.. _api-verger:

`verger`
========

.. currentmodule:: jouets.verger

Fonctions définies pour le :ref:`calcul de probabilités au jeu du verger <doc-verger>`.

.. autofunction:: probabilite

Stratégies
----------

.. autodata:: STRATEGIES

.. autofunction:: panier_max
.. autofunction:: panier_min
.. autofunction:: panier_random
