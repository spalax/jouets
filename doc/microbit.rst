..
   Copyright 2019-2023 Louis Paternault
   
   Cette œuvre de Louis Paternault est mise à disposition selon les termes de
   la licence Creative Commons Attribution - Partage dans les Mêmes Conditions
   4.0 International (CC-BY-SA). Le texte complet de la licence est disponible
   à l'adresse : http://creativecommons.org/licenses/by-sa/4.0/deed.fr

*********************************************************
`microbit` — Différents programmes pour carte `micro:bit`
*********************************************************

Pour me préparer à l'enseignement de la `SNT <http://snt.ababsurdo.fr>`__
(en seconde générale),
j'ai écrit quelques programmes pour carte `micro:bit <http://microbit.org>`__.

Tous les fichiers sont situés dans le répertoire `microbit <https://framagit.org/spalax/jouets/tree/main/microbit>`__ du dépôt.

.. toctree::
   :maxdepth: 1

   microbit/boussole
   microbit/chronometre
   microbit/memoire
   microbit/niveau
   microbit/serpent
