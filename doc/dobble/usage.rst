..
   Copyright 2014-2015 Louis Paternault
   
   Cette œuvre de Louis Paternault est mise à disposition selon les termes de
   la licence Creative Commons Attribution - Partage dans les Mêmes Conditions
   4.0 International (CC-BY-SA). Le texte complet de la licence est disponible
   à l'adresse : http://creativecommons.org/licenses/by-sa/4.0/deed.fr

.. _dobble_usage:

Générateur de jeux
==================

.. argparse::
    :module: jouets.dobble.__main__
    :func: analyse
    :prog: dobble

