Changelogs des sous-programmes :

* [addition](http://framagit.org/spalax/jouets/blob/main/changelogs/addition.md)
* [anagrammes](http://framagit.org/spalax/jouets/blob/main/changelogs/anagrammes.md)
* [attente](http://framagit.org/spalax/jouets/blob/main/changelogs/anagrammes.md)
* [aperitif](http://framagit.org/spalax/jouets/blob/main/changelogs/aperitif.md)
* [azul](http://framagit.org/spalax/jouets/blob/main/changelogs/azul.md)
* [bataille](http://framagit.org/spalax/jouets/blob/main/changelogs/bataille.md)
* [cellulaire](http://framagit.org/spalax/jouets/blob/main/changelogs/cellulaire.md)
* [chemin](http://framagit.org/spalax/jouets/blob/main/changelogs/chemin.md)
* [dobble](http://framagit.org/spalax/jouets/blob/main/changelogs/dobble.md)
* [euler](http://framagit.org/spalax/jouets/blob/main/changelogs/euler.md)
* [egyptienne](http://framagit.org/spalax/jouets/blob/main/changelogs/egyptienne.md)
* [erathostene](http://framagit.org/spalax/jouets/blob/main/changelogs/erathostene.md)
* [fractale](http://framagit.org/spalax/jouets/blob/main/changelogs/fractale.md)
* [horloge](http://framagit.org/spalax/jouets/blob/main/changelogs/horloge.md)
* [labyrinthe](http://framagit.org/spalax/jouets/blob/main/changelogs/labyrinthe.md)
* [latex](http://framagit.org/spalax/jouets/blob/main/changelogs/latex.md)
* [mafia](http://framagit.org/spalax/jouets/blob/main/changelogs/mafia.md)
* [microbit](http://framagit.org/spalax/jouets/blob/main/changelogs/microbit.md)
* [mpa](http://framagit.org/spalax/jouets/blob/main/changelogs/mpa.md)
* [panini](http://framagit.org/spalax/jouets/blob/main/changelogs/panini.md)
* [pestecholera](http://framagit.org/spalax/jouets/blob/main/changelogs/pestecholera.md)
* [sudoku](http://framagit.org/spalax/jouets/blob/main/changelogs/sudoku.md)
* [traitementimage](http://framagit.org/spalax/jouets/blob/main/changelogs/traitementimage.md)
* [truchet](http://framagit.org/spalax/jouets/blob/main/changelogs/truchet.md)
* [verger](http://framagit.org/spalax/jouets/blob/main/changelogs/verger.md)

---

* Version 0.6.0 (2024-12-27)

    * Support de Python
        * Abandon du support de python3.10.
        * Ajout du support de python3.13
    * Nouveaux programmes :
        * panini 0.1.0 : Calcul du nombre d'achats nécessaires pour compléter un album Panini.
        * mpa 0.1.0 : Calcul du nombre de chemins dans un livre « Ma Première Aventure ».

    -- Louis Paternault <spalax@gresille.org>

* Version 0.5.1 (2023-10-07)

    * Correction de liens cassés dans la documention.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.5.0 (2023-10-07)

    * Nouveaux programmes :
        * latex 0.1.0 : Morceaux de code amusants en LaTeX.
        * truchet 0.1.0 : Génération de tuiles de Truchet généralisées
    * Mise à jour des programmes :
        * microbit 0.2.0 : (nouveau programme : serpent).
    * Abandon de python3.8 et python3.9
    * Support de python3.12.

    -- Louis Paternault <spalax@gresille.org>

* Version 0.4.0 (2022-11-29)

    * Support de python3.8 à python3.11.
    * Abandon du support de python3.7.
    * Nouveaux programmes :
        * horloge 0.1.0

    -- Louis Paternault <spalax@gresille.org>

* Version 0.3.0 (2020-04-27)

    * Support de python3.7.
    * Abandon du support de python3.4, python3.5, et python3.6.
    * Amélioration (et simplification) du système intégré de plugins (#16).
    * Amélioration et partage du système de mise en cache (#15).
    * Nouveaux programmes :
        * addition 0.1.0
        * attente 0.1.0
        * anagrammes 0.1.0
        * azul 0.1.0
        * cellulaire 0.1.0
        * euler 0.1.0
        * bataille 0.1.0
        * microbit 0.1.0
        * pygame 0.1.0
        * traitementimage 0.1.0
        * verger 0.1.0
      * Mise à jour des programmes :
        * cellulaire 0.1.1
        * dobble 0.2.1
        * erathostene 0.3.0
        * euler 0.1.1
        * fractale 0.2.1
        * labyrinthe 0.2.1
        * mafia 0.1.1
        * pestecholera 0.2.1

    -- Louis Paternault <spalax@gresille.org>

* Version 0.2.0 (2016-07-23)

    * Les programmes peuvent être appelés par leur module python (par exemple ``python -m foo ARGS`` en plus de ``foo ARGS``).
    * Diverses améliorations des bibliothèques communes.
    * Nouveau programme :
        * mafia 0.1.0
    * Mise à jour des programmes :
        * aperitif 0.2.0
        * chemin 0.2.0
        * dobble 0.2.0
        * egyptienne 0.2.0
        * erathostene 0.2.0
        * fractale 0.2.0
        * labyrinthe 0.2.0
        * pestecholera 0.2.0
        * sudoku 0.2.0

    -- Louis Paternault <spalax@gresille.org>

* Version 0.1.0 (2015-03-20)

    * Première version publiée
    * Version des sous-programmes :
        * aperitif 0.1.0
        * chemin 0.1.0
        * dobble 0.1.0
        * egyptienne 0.1.0
        * erathostene 0.1.0
        * fractale 0.1.0
        * labyrinthe 0.1.0
        * pestecholera 0.1.0
        * sudoku 0.1.3

    -- Louis Paternault <spalax@gresille.org>
